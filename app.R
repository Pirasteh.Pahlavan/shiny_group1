#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(tidyverse)
library(maps)
library(mapproj)

overdoses       <- read_csv('overdoses.csv')
prescriber_info <- read_csv('prescriber_info.csv')
opioids         <- read.csv('opioids.csv')
opioids$medication <- gsub("\ |-",".", as.character(opioids$Drug.Name))
map <- map_data("state")

# Create long dataset of opioid prescriptions
prescription <- semi_join(
    prescriber_info %>%
        select(-Gender,-Credentials, -Specialty) %>% 
        pivot_longer(-c(State, NPI), 
                     names_to  = 'medication', 
                     values_to = 'value') %>% 
        arrange(State, medication) ,
    opioids,
    by = "medication"
) # semi_join


# Calculate total opioid prescription by state
prescOpioids <- prescription %>% 
    group_by(State) %>% 
    summarize(totalOpioids = sum(value))

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("Opioid Data"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            sliderInput("deathRateLimit",
                        "Death Rate Limit:",
                        min = 0,
                        max = 350,
                        value = 0)
        ),

        # Show a plot of the generated distribution
        mainPanel(
           plotOutput("ScatterPlot"), 
           plotOutput("Map")
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

    output$ScatterPlot <- renderPlot({
        
        # Merge opioid deaths with opioid prescription
        deathRateLimit <- input$deathRateLimit
        data <- left_join(
            overdoses,
            prescOpioids,
            by = c("Abbrev"="State")
        ) %>% 
            mutate(region=tolower(State)) %>% 
            mutate(deathRate  = Deaths/Population*10^6, 
                   opioidRate = totalOpioids/Population*10^6) %>% 
            mutate(deathRate2 = ifelse(deathRate>=deathRateLimit,deathRate,NA),
                   opioidRate2= ifelse(deathRate>=deathRateLimit,opioidRate,NA),
                   Abbrev2    = ifelse(deathRate>=deathRateLimit,Abbrev,NA))
        
        # generate bins based on input$bins from ui.R
        ggplot(data, aes(x=opioidRate, y=deathRate)) +
            geom_point() + 
            geom_hline(aes(yintercept=deathRateLimit)) +
            geom_point(aes(x=opioidRate2, y=deathRate2, color='red')) +
            ylim(c(0,350)) + 
            xlim(c(0,15000)) +
            labs(y="Deaths per million", x="Prescriptions per million",
                 title="U.S. opioid crisis",
                 subtitle="Opioid deaths vs. opioid prescriptions by state") +
            geom_text(aes(label=Abbrev2,color='red'), nudge_y=10)
        
    })
    output$Map <- renderPlot( 
        {
            # Merge opioid deaths with opioid prescription
            deathRateLimit <- input$deathRateLimit
            data <- left_join(
                overdoses,
                prescOpioids,
                by = c("Abbrev"="State")
            ) %>% 
                mutate(region=tolower(State)) %>% 
                mutate(deathRate  = Deaths/Population*10^6, 
                       opioidRate = totalOpioids/Population*10^6) %>% 
                mutate(deathRate2 = ifelse(deathRate>=deathRateLimit,deathRate,NA),
                       opioidRate2= ifelse(deathRate>=deathRateLimit,opioidRate,NA),
                       Abbrev2    = ifelse(deathRate>=deathRateLimit,Abbrev,NA))
            ggplot() +
                geom_map(data=map, map=map, aes(x=long, y=lat, map_id=region)) +
                geom_map(data=data, map=map, aes(fill=deathRate2, map_id=region)) +
                geom_text(data=data.frame(state.center,data$Abbrev2),
                          aes(x=x, y=y,label=data.Abbrev2),
                          size=3) +
                scale_fill_continuous(low='thistle2', high='darkred', 
                                      guide='colorbar') +
                theme(panel.border = element_blank()) + 
                theme(panel.background = element_blank()) + 
                theme(axis.ticks = element_blank()) +
                theme(axis.text = element_blank()) +
                labs(x=NULL, y=NULL, fill="Deaths/million") 
            
        })
}

# Run the application 
shinyApp(ui = ui, server = server)
